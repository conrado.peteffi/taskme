import dayjs from 'dayjs';
import React, {
  memo,
  useContext,
  useState,
  createContext,
  useMemo,
  // useCallback,
} from 'react';

const mockinitialconfig = {
  "type": "initialConfig",
  "startTime": dayjs("2023-06-21T03:00:00.000Z"),
  "endTime": dayjs("2023-06-21T18:00:00.000Z")
};

const mockvalues = [
  {
      "type": "meeting",
      "startTime": dayjs("2023-06-21T04:00:00.000Z"),
      "endTime": dayjs("2023-06-21T05:00:00.000Z"),
      "name": "1"
  },
  {
      "type": "meeting",
      "startTime": dayjs("2023-06-21T05:30:00.000Z"),
      "endTime": dayjs("2023-06-21T06:00:00.000Z"),
      "name": "2"
  },
  {
      "type": "meeting",
      "startTime": dayjs("2023-06-21T15:00:00.000Z"),
      "endTime": dayjs("2023-06-21T16:00:00.000Z"),
      "name": "3"
  }
];

export const TaskMeContext = createContext();
const getDuration = (a, b) => Math.abs(Math.floor(a?.diff(b, 'minute', true)));

export const TaskMeProvider = memo(({ children }) => {
  const [initialConfig, setInitialConfig] = useState(undefined);
  const [values, setValues] = useState([]);
  const [isSystemValuesGenerated, setIsSystemValuesGenerated] = useState(false);
  const currentValues = values
    .sort((a, b) => (a.startTime.isBefore(b.startTime) ? -1 : 1));

  const applyMock = () => {
    setValues(mockvalues);
    setInitialConfig(mockinitialconfig);
    calculateSystemTasks();
  }

  const startTimeFreeMoment = {
    duration: getDuration(initialConfig?.startTime, currentValues?.[0]?.startTime),
  };
  const endTimeFreeMoment = {
    duration: getDuration(currentValues?.[currentValues?.length - 1]?.endTime, initialConfig?.endTime),
  };
  const currentFreeMoments = [
    startTimeFreeMoment,
    ...currentValues?.map((task, idx, arr) => {
      let freeMoment = { duration: 0, realIdx: idx };
      if (arr[idx + 1]) {
        freeMoment.duration = getDuration(task?.endTime, arr[idx + 1]?.startTime);
      }
      return freeMoment;
    }),
    endTimeFreeMoment,
  ];

  const totalTime = getDuration(initialConfig?.startTime, initialConfig?.endTime);
  const totalInitialTasksTime = currentValues?.map((task) => getDuration(task.startTime, task.endTime))?.reduce((cur, acc) => (acc + cur), 0);
  const totalFreeTime = totalTime - totalInitialTasksTime;

  const getHasAvailableFocusTime = (final) => final
    ?.filter((value) => value.type === 'focusTime')
    ?.map((value) => value?.duration)
    ?.reduce((acc, cur) => (acc + cur), 0);

  const createSystemTask = (nextFreeTimeDuration, previousEndTime, isFocusTime) => {
    if (nextFreeTimeDuration > 0) {
      const newDuration = getDuration(previousEndTime, previousEndTime.add(nextFreeTimeDuration, 'minute'));
      
      return {
        type: isFocusTime ? 'focusTime' : 'multiTask',
        name: isFocusTime ? 'Momento de Foco' : 'Planejamento / Multi-Task',
        startTime: previousEndTime,
        endTime: previousEndTime.add(nextFreeTimeDuration, 'minute'),
        duration: newDuration,
        isSystemTask: true,
      };
    }
    return undefined;
  }

  const generateSystemTasksFromFreeTime = (nextFreeTime, endTime, availableFocusTime) => {
    let currentAvailableFocusTime = availableFocusTime;
    const defaultMaxTimeToBeFocus = 120;
    let arr = [];
    if (nextFreeTime.duration > defaultMaxTimeToBeFocus) {
      let newTimes = [];
      let curValue = 0;
      while (curValue <= nextFreeTime.duration) {
        if ((curValue + defaultMaxTimeToBeFocus) <= nextFreeTime.duration) {
          newTimes = [...newTimes, {
            duration: defaultMaxTimeToBeFocus,
          }];
        } else {
          newTimes = [...newTimes, {
            duration: (nextFreeTime.duration - curValue),
          }];
        }
        curValue = curValue + defaultMaxTimeToBeFocus;
      }
      let currentEndTime = endTime;
      newTimes.forEach((newTime) => {
        const isFocusTime = newTime.duration >= 50 && (currentAvailableFocusTime < 240);
        if (isFocusTime) {
          currentAvailableFocusTime = currentAvailableFocusTime + newTime.duration
        }
        const newTask = createSystemTask(newTime.duration, currentEndTime, isFocusTime);
        if (newTask) {
          arr = [...arr, newTask];
          currentEndTime = currentEndTime.add(newTime.duration, 'minute');
        }
      })
    } else {
      const isFocusTime = nextFreeTime.duration >= 50 && (currentAvailableFocusTime < 240);
      if (isFocusTime) {
        currentAvailableFocusTime = currentAvailableFocusTime + nextFreeTime.duration
      }
      const newTask = createSystemTask(nextFreeTime.duration, endTime, isFocusTime);
      arr = [...arr, newTask];
    }
    return arr.filter((item) => item);
  }

  const calculateSystemTasks = () => {
    if (initialConfig) {
      let availableFocusTime = 0;
      let final = [...generateSystemTasksFromFreeTime(startTimeFreeMoment, initialConfig.startTime, availableFocusTime)];
      availableFocusTime = getHasAvailableFocusTime(final);
      currentValues.forEach((value, idx) => {
        final = [...final, value];
        const nextFreeTime = currentFreeMoments.find((item) => idx === item.realIdx);
        final = [...final, ...generateSystemTasksFromFreeTime(nextFreeTime, value.endTime, availableFocusTime)];
        availableFocusTime = getHasAvailableFocusTime(final);
      });
      final = [
        ...final,
        ...generateSystemTasksFromFreeTime(endTimeFreeMoment, currentValues[currentValues.length - 1].endTime, availableFocusTime),
      ];
      if (final.length > currentValues.length) {
        setValues(final);
        setIsSystemValuesGenerated(true);
      }
    }
  };

  const contextValues = useMemo(() => ({
    initialConfig,
    setInitialConfig,
    values,
    setValues,
    currentValues,
    calculateSystemTasks,
    isSystemValuesGenerated,
    applyMock,
  }), [currentValues, initialConfig, setInitialConfig, values, setValues]);

  return (
    <TaskMeContext.Provider
      value={contextValues}
    >
      {children}
    </TaskMeContext.Provider>
  );
});

export const useTaskMe = () => useContext(TaskMeContext);
