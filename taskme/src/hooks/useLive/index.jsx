/* eslint-disable max-len */
import { useRef, useState } from 'react';
import dayjs from 'dayjs';
import {
  useTaskMe,
} from 'app/hooks';
import { useEffect, useMemo } from 'react';

const getDuration = (a, b) => Math.abs(Math.floor(a?.diff(b, 'minute', true)));

const meetingMessages = [
  'Aproveite para beber água!',
  'Procure sentar-se com postura adequada: coluna reta e apoiada no assento, altura do monitor na linha dos olhos e com os dois pés apoiados no chão.',
  'Procure sentar-se com postura adequada: braços apoiados na mesa e retos na altura dos cotovelos, de modo a destencionar os ombros e o pescoço.',
  'Procure dedicar sua total atenção. Os momentos de foco e multi-task serão aproveitados em outro momento.',
  'Procure dedicar sua total atenção. Se sua presença foi requisitada, é porque você pode contribuir.',
  'Procure dedicar sua total atenção. Às vezes, não haverá melhor momento para falar sobre esse assunto.',
];

const preparationMessages = [
  'Este é o momento para você pensar no que vai fazer. Organize suas ideias para ganhar tempo.',
  'Já pensou por onde vai começar? Organize seus planos e não esqueça de evitar as distrações durante o momento de foco.',
  'Respire fundo, organize as ideias e pegue uma garrafa de água. O seu momento de foco já vai inciar.',
];

const breakMessages = [
  'O momento agora é de descansar a mente. Beba água, vá ao banheiro, respire.',
  'Parabéns! Agora descanse um pouco. Pausas são fundamentais para voltarmos energizados ao trabalho.',
  'Que tal pegar um pouco de sol e respirar fundo? Aproveite para descansar um pouco.',
  'Que tal um lanche rápido? Aproveite esse momento para relaxar e voltar mais energizado.',
];

const multiTaskMessages = [
  'Este é um momento em que você pode lidar com mais de um assunto ao mesmo tempo. Aproveite para planejar o seu dia e suas próximas tarefas!',
  'Aqui está um bom momento para alinhar dúvidas com os colegas, planejar suas atividades ou realizar testes naquela task anterior.',
  'Um momento propício para estudar alguma documentação, compartilhar ideias com os colegas ou planejar as próximas reuniões de alinhamento com o time.',
];

const focusTimeMessages = [
  'Aqui está seu momento de foco total. Desligue os aparelhos que podem distraí-lo e coloque a mão na massa!',
  'O objetivo aqui é cumprir suas tarefas principais e nada mais. Dedique esse momento exclusivamente para conquistar o seu objetivo no dia.',
  'Chegou a hora de ligar o modo "ocupado". Nos minutos a seguir, não faça nada que não esteja diretamente relacionado com as suas tarefas principais para o dia de hoje.'
];

const specialMessage = 'O dia já está terminando e com isso seu escritório também está mais escuro. Já conferiu a iluminação do seu monitor? O brilho alto e o excesso de luz azul podem prejudicar seu sono a noite.';

const personalMessage = 'Momento reservado para resolver assunto pessoal.';
const otherMessage = 'Reserve esse momento para lidar com o assunto especificado.';

const liveTypeMapper = {
  meeting: 'card',
  personal: 'card',
  other: 'card',
  break: 'modal',
  focusTime: 'card',
  // to be generated
  focusTimePlanning: 'modal',
  // to be generated
  pause: 'modal',
  multiTask: 'card',
  special: 'modal',
};

const getLiveTask = (task = {}, routineStartTime, conditions = {}) => {
  // LIVE TYPE
  let liveType = '';
  if (conditions.isStartTime) {
    liveType = 'line';
    task.name = 'Início da rotina';
  } else if (conditions.isEndTime) {
    liveType = 'lineEnd';
    task.name = 'Fim da rotina';
    task.title = 'Fim da rotina';
    task.description = 'Fim da rotina!'
  } else {
    liveType = liveTypeMapper[task.type];
  }

  // DESCRIPTION
  let description = '';
  if (task.type === 'meeting') {
    description = meetingMessages[Math.floor(Math.random()*meetingMessages.length)];
  } else if (task.type === 'personal') {
    description = personalMessage;
  } else if (task.type === 'other') {
    description = otherMessage;
  } else if (task.type === 'break' || task.type === 'pause') {
    description = breakMessages[Math.floor(Math.random()*breakMessages.length)];
  } else if (task.type === 'focusTimePlanning') {
    description = preparationMessages[Math.floor(Math.random()*preparationMessages.length)];
  } else if (task.type === 'multiTask') {
    description = multiTaskMessages[Math.floor(Math.random()*multiTaskMessages.length)];
  } else if (task.type === 'special') {
    description = specialMessage;
  } else if (task.type === 'focusTime') {
    description = focusTimeMessages[Math.floor(Math.random()*focusTimeMessages.length)];
  }

  // WHEN TO HAPPEN
  let startTime = task.startTime;
  if (conditions.isEndTime) {
    startTime = task.endTime;
  }
  const whenToHappen = getDuration(routineStartTime, startTime);

  return {
    ...task,
    liveType,
    description,
    whenToHappen,
  };
};

const useLive = () => {
  // TIME LOGIC

  const initialTime = dayjs().set('second', 0).set('minute', 0).set('hour', 0);
  const [time, setTime] = useState(initialTime);
  const timeSinceBeginning = getDuration(initialTime, time);

  const {
    initialConfig,
    currentValues,
  } = useTaskMe();

  let tasks = [];
  let modifiedCurrentValues = [];

  if (initialConfig && !!currentValues.length) {
    currentValues?.forEach((value) => {
      if (value.type === 'focusTime') {
        const focusTimePlanning = {
          type: "focusTimePlanning",
          startTime: value.startTime,
          endTime: value.startTime.add(5, 'minute'),
        };
        const focusTime = {
          ...value,
          startTime: focusTimePlanning.endTime,
          endTime: value.endTime.subtract(15, 'minute'),
        };
        const pause = {
          type: "pause",
          startTime: focusTime.endTime,
          endTime: value.endTime,
        };
        modifiedCurrentValues = [
          ...modifiedCurrentValues,
          getLiveTask(focusTimePlanning, initialConfig.startTime),
          getLiveTask(focusTime, initialConfig.startTime),
          getLiveTask(pause, initialConfig.startTime),
        ];
      } else if (value.type === 'multiTask' && getDuration(value.startTime, value.endTime) >= 50) {
        const multiTask = {
          ...value,
          startTime: value.startTime,
          endTime: value.endTime.subtract(15, 'minute'),
        };
        const pause = {
          type: "pause",
          startTime: multiTask.endTime,
          endTime: value.endTime,
          name: "Pausa do momento mulit-task",
        };
        modifiedCurrentValues = [
          ...modifiedCurrentValues,
          getLiveTask(multiTask, initialConfig.startTime),
          getLiveTask(pause, initialConfig.startTime),
        ];
      } else {
        modifiedCurrentValues = [
          ...modifiedCurrentValues,
          getLiveTask(value, initialConfig.startTime),
        ];
      }
    });
  }

  tasks = useMemo(() => ([
    getLiveTask(initialConfig, initialConfig?.startTime, { isStartTime: true }),
    ...modifiedCurrentValues,
    getLiveTask(initialConfig, initialConfig?.startTime, { isEndTime: true }),
  ]), []);

  // tasks = [
  //   getLiveTask(initialConfig, initialConfig?.startTime, { isStartTime: true }),
  //   ...modifiedCurrentValues,
  //   getLiveTask(initialConfig, initialConfig?.startTime, { isEndTime: true }),
  // ];

  const [liveArr, setLiveArr] = useState([]);
  const add1Min = useRef(false);
  const add15Min = useRef(false);
  const add60Min = useRef(false);

  useEffect(() => {
    setTimeout(() => {
      let newTime = time.add(1, 'second');
      if (add1Min.current) {
        newTime = time.add(1, 'minutes');
        add1Min.current = false;
      }
      if (add15Min.current) {
        newTime = time.add(15, 'minutes');
        add15Min.current = false;
      }
      if (add60Min.current) {
        newTime = time.add(60, 'minutes');
        add60Min.current = false;
      }
      setTime(newTime);
    }, 1000);

    if (initialConfig && !!currentValues.length) {
      const currentLiveArr = tasks.filter((task) => task.whenToHappen <= timeSinceBeginning).map((task, idx, arr) => {
        // if ((idx === arr.length - 2 && (task.type === 'focusTime' || task.type === 'multiTask')) && arr[idx + 1].type === 'pause') {
        //   return {
        //     ...task,
        //     isActive: true,
        //   };
        // } else if (idx === arr.length - 1) {
        //   return {
        //     ...task,
        //     isActive: true,
        //   };
        // } return task;
        if (idx === arr.length - 1) {
          return {
            ...task,
            isActive: true,
          };
        } return task;
      });

      // console.log(currentLiveArr)
  
      setLiveArr(currentLiveArr);
    }
  }, [time]);

  const timeToDisplay = time.format('HH:mm:ss');

  const setAdd1Min = () => {
    add1Min.current = true;
  }
  const setAdd15Min = () => {
    add15Min.current = true;
  }
  const setAdd60Min = () => {
    add60Min.current = true;
  }

  return {
    timeToDisplay,
    liveArr,
    setAdd1Min,
    setAdd15Min,
    setAdd60Min,
  };
};

export default useLive;
