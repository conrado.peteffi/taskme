import { useState } from 'react';

const useSteps = (stepsMapper) => {
  const [step, setStep] = useState(0);
  const goToNextStep = () => setStep((prev) => prev + 1);
  const Component = stepsMapper[step];
  return {
    goToNextStep,
    Component,
    step,
  };
};

export default useSteps;
