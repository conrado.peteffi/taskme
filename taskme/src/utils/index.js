export const typeLabelMapper = {
  meeting: 'Reunião',
  break: 'Intervalo',
  personal: 'Compromisso Pessoal',
  other: 'Outro',
  focusTime: 'Momento de foco',
  multiTask: 'Multi-Task',
  focusTimePlanning: 'Planejamento do momento de foco',
  pause: 'Pausa'
};
