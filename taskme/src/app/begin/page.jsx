/* eslint-disable react/jsx-props-no-spreading */
'use client';
import React from 'react';
import {
  useSteps,
} from 'app/hooks';
import {
  Step1,
  Step2,
} from './steps';
import Begin from './styles';

const stepsMapper = {
  0: (props) => <Step1 {...props} />,
  // 0: (props) => <Step2 {...props} />,
  1: (props) => <Step2 {...props} />,
};

function BeginPage() {
  const {
    goToNextStep,
    Component,
    step,
  } = useSteps(stepsMapper);
  return (
    <Begin fullHeight={step === 0}>
      <Component goToNextStep={goToNextStep} />
      {/* <button onClick={goToNextStep}>next step</button> */}
    </Begin>
  );
}

export default BeginPage;
