import React, {
  useEffect,
  useState,
} from 'react';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import TextField from '@mui/material/TextField';
import { TimePicker } from '@mui/x-date-pickers/TimePicker';
import { MobileTimePicker } from '@mui/x-date-pickers/MobileTimePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import dayjs from 'dayjs';
import Alert from '@mui/material/Alert';

export default function DialogComponent({
  open,
  handleClose,
  handleChange,
  initialConfig,
  handleIntialConfig,
  validate,
  editionObject = {
    editionTask: undefined,
    editionName: undefined,
    editionStartTime: undefined,
    editionEndTime: undefined,
  },
  setEditionObject,
  handleEdit,
}) {
  const {
    editionTask,
    editionName,
    editionStartTime,
    editionEndTime,
  } = editionObject;
  const [task, setTask] = useState('meeting');
  const [name, setName] = useState(undefined);
  const [startTime, setStartTime] = useState(undefined);
  const [endTime, setEndTime] = useState(undefined);
  const [warning, setWarning] = useState(undefined);
  const validation = validate({ startTime, endTime, name }, editionObject);

  useEffect(() => {
    if (Object.values(editionObject)?.some((value) => value)) {
      debugger
      setTask(editionTask);
      setName(editionName);
      setStartTime(editionStartTime);
      setEndTime(editionEndTime);
    }
  }, [editionEndTime, editionName, editionObject, editionStartTime, editionTask]);

  useEffect(() => {
    if (!validation.isValid) {
      setWarning(validation.message);
    } else {
      setWarning(undefined);
    }
  }, [startTime, endTime, name]);

  const resetValues = () => {
    setTask('meeting');
    setStartTime(undefined);
    setEndTime(undefined);
    setName(undefined);
    setEditionObject(undefined);
  };

  console.log(startTime);

  console.log(endTime)

  console.log(validation.isValid)


  return (
    <Dialog disableEscapeKeyDown open={open} onClose={handleClose}>
      <DialogTitle>{!initialConfig ? 'Defina o horário de expediente' : 'Adicionar Atividade'}</DialogTitle>
      <DialogContent>
        {!initialConfig ? (
          <Box
            component="form"
            sx={{
              display: 'flex',
              flexDirection: 'column',
              m: 'auto',
              width: 'fit-content',
            }}
          >
            <FormControl sx={{ m: 1 }}>
              {/* <InputLabel htmlFor="demo-dialog-native">Tipo de atividade</InputLabel> */}
              <TextField
                id="outlined-basic"
                variant="outlined"
                label="Tipo de atividade"
                defaultValue="Período de expediente"
                InputProps={{
                  readOnly: true,
                }}
              />
              {/* <Select
                defaultValue="initialConfig"
                onChange={(e) => setTask(e.target.value)}
                input={<OutlinedInput label="Tipo de atividade" id="demo-dialog-native" />}
                readOnly
              >
                <MenuItem value="initialConfig">Período de expediente</MenuItem>
              </Select> */}
            </FormControl>
            <FormControl sx={{ m: 1 }}>
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <TimePicker
                  ampm={false}
                  label="Horário de inicio"
                  // defaultValue={dayjs("09:00:00", "HH:mm:ss")}
                  value={startTime}
                  format="HH:mm"
                  onChange={(value) => setStartTime(dayjs(value, 'HH:mm').set('millisecond', 0))}
                  maxTime={endTime}
                />
              </LocalizationProvider>
            </FormControl>
            <FormControl sx={{ m: 1 }}>
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <TimePicker
                  ampm={false}
                  label="Horário de fim"
                  // defaultValue={dayjs("09:00:00", "HH:mm:ss")}
                  value={endTime}
                  format="HH:mm"
                  onChange={(value) => setEndTime(dayjs(value, 'HH:mm').set('millisecond', 0))}
                  minTime={startTime}
                />
              </LocalizationProvider>
            </FormControl>
          </Box>
        ) : (
          <Box
            component="form"
            sx={{
              display: 'flex',
              flexDirection: 'column',
              m: 'auto',
              width: 'fit-content',
            }}
          >
            <FormControl sx={{ m: 1 }}>
              <InputLabel htmlFor="demo-dialog-native">Tipo de atividade</InputLabel>
              <Select
                native
                value={task}
                onChange={(e) => setTask(e.target.value)}
                input={<OutlinedInput label="Tipo de atividade" id="demo-dialog-native" />}
              >
                <option value="meeting">Reunião</option>
                <option value="break">Intervalo</option>
                <option value="personal">Compromisso pessoal</option>
                <option value="other">Outro</option>
              </Select>
            </FormControl>
            <FormControl sx={{ m: 1 }}>
              <TextField
                id="outlined-basic"
                variant="outlined"
                label="Nome da atividade"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </FormControl>
            <FormControl sx={{ m: 1 }}>
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <TimePicker
                  ampm={false}
                  style={{ minWidth: '300px' }}
                  label="Horário de inicio"
                  // defaultValue={dayjs("09:00:00", "HH:mm:ss")}
                  value={startTime}
                  format="HH:mm"
                  onChange={(value) => setStartTime(dayjs(value, 'HH:mm').set('millisecond', 0))}
                  minTime={initialConfig.startTime}
                  maxTime={endTime || initialConfig.endTime}
                />
              </LocalizationProvider>
            </FormControl>
            <FormControl sx={{ m: 1 }}>
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <TimePicker
                  ampm={false}
                  label="Horário de fim"
                  // defaultValue={dayjs("09:00:00", "HH:mm:ss")}
                  value={endTime}
                  format="HH:mm"
                  onChange={(value) => setEndTime(dayjs(value, 'HH:mm').set('millisecond', 0))}
                  maxTime={initialConfig.endTime}
                  minTime={startTime || initialConfig.startTime}
                />
              </LocalizationProvider>
            </FormControl>
            {warning && (
              <Alert style={{ maxWidth: '240px', margin: '9px' }} severity="error">{warning}</Alert>
            )}
          </Box>
        )}
      </DialogContent>
      <DialogActions>
        <Button onClick={() => {
          handleClose();
          setEditionObject(undefined);
        }}
        >
          Cancel
        </Button>
        <Button
          disabled={(!startTime || !endTime) || !validation.isValid}
          onClick={() => {
            if (!initialConfig) {
              handleIntialConfig({
                type: 'initialConfig',
                startTime,
                endTime,
                // name: 'teste',
              });
            } else {
              const newValue = {
                type: task,
                startTime,
                endTime,
                name,
              };
              if (Object.values(editionObject)?.some((value) => value)) {
                handleEdit(newValue, editionObject);
              } else {
                handleChange(newValue);
              }
            }
            resetValues();
            handleClose();
          }}
        >
          Ok
        </Button>
      </DialogActions>
    </Dialog>
  );
}
