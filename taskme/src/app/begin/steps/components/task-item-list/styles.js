import styled from 'styled-components';

const TaskItemList = styled.div`
  display: flex;
  min-height: 60px;
  height: fit-content;
  border-radius: 25px;
  margin: 10px;
  padding: 5px 0;
  background-color: ${({ isSystemTask }) => isSystemTask ? 'rgba(84, 63, 149, 2)' : 'rgba(67, 61, 85, 0.2)'};
  margin-bottom: 12px;
  ${({ isFirst }) => isFirst && ('margin-top: 12px;')}
  overflow-wrap: break-word;
  text-align: center;
`;

TaskItemList.EqualDivisionContainer = styled.div`
  justify-content: center;
  align-items: center;
  position: relative;
  display: flex;
  flex: 1;
  flex-direction: column;
  ${({ isLast }) => !isLast && 'border-right: 1px solid white;'}
`;

TaskItemList.EqualDivisionContainer.Title = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-weight: 400;
  font-size: 16px;
  line-height: 21px;
`;

TaskItemList.EqualDivisionContainer.Content = styled.div`
  padding: 5px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-weight: 400;
  font-size: 20px;
  line-height: 31px;
`;

export default TaskItemList;
