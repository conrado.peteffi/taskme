import React, { useState } from 'react';
import TaskItemList from './styles';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import FormControl from '@mui/material/FormControl';
import dayjs from 'dayjs';
import {
  RiPencilLine,
  RiDeleteBinLine,
} from 'react-icons/ri';
import IconButton from '@mui/material/IconButton';
import {
  useTaskMe,
} from 'app/hooks';

// const theme = createTheme({
//   components: {
//     // Name of the component
//     Select: {
//       styleOverrides: {
//         // Name of the slot
//         root: {
//           // Some CSS
//           color: 'white',
//         },
//       },
//     },
//   },
// });

const typeLabelMapper = {
  meeting: 'Reunião',
  break: 'Intervalo',
  personal: 'Compromisso Pessoal',
  other: 'Outro',
  focusTime: 'Momento de foco',
  multiTask: 'Multi-Task',
};

export default function TaskItemListComponent({
  type = '',
  name = '',
  startTime = '',
  endTime = '',
  isFirst,
  onEdit,
  onDelete,
  isSystemTask,
}) {
  // const {
  //   setValues,
  //   values,
  // } = useTaskMe();
  // const [curType, setCurType] = useState(type);
  // const [curName, setCurName] = useState(name);
  // const [curStartTime, setCurStartTime] = useState(startTime);
  // const [curEndTime, setCurEndTime] = useState(endTime);
  // const [curIsEditing, setCurIsEditing] = useState(isEditing);

  // const onDelete = () => {
  //   const idx = values.map((value) => value?.name).indexOf(name);
  //   const newArr = values;
  //   newArr.splice(idx, 1);
  //   setValues(newArr);
  // }

  const handleEdit = () => {
    onEdit({
      editionTask: type,
      editionName: name,
      editionStartTime: startTime,
      editionEndTime: endTime,
    });
  };

  return (
    <TaskItemList isSystemTask={isSystemTask} isFirst={isFirst}>
      <TaskItemList.EqualDivisionContainer>
        <TaskItemList.EqualDivisionContainer.Title>
          {isSystemTask ? '' : typeLabelMapper[type]}
        </TaskItemList.EqualDivisionContainer.Title>
        <TaskItemList.EqualDivisionContainer.Content>
          {name || '-'}
        </TaskItemList.EqualDivisionContainer.Content>
      </TaskItemList.EqualDivisionContainer>
      {/* <FormControl style={{ display: 'flex', flexDirection: 'row' }} fullWidth> */}
      {/* <TaskItemList.EqualDivisionContainer>
        <InputLabel style={{ color: 'white' }} id="demo-simple-select-label">Atividade</InputLabel>
        <Select
          variant='standard'
          style={{ color: 'white' }}
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={curType}
          onChange={(event) => setCurType(event.target.value)}
          label="Atividadeeee"
          readOnly={!curIsEditing}
        >
          <MenuItem value="meeting">Reunião</MenuItem>
          <MenuItem value="other">Outro</MenuItem>
        </Select>
      </TaskItemList.EqualDivisionContainer> */}

      <TaskItemList.EqualDivisionContainer>
        <TaskItemList.EqualDivisionContainer.Title>
          início
        </TaskItemList.EqualDivisionContainer.Title>
        <TaskItemList.EqualDivisionContainer.Content>
          {dayjs(startTime).format('HH:mm')}
        </TaskItemList.EqualDivisionContainer.Content>
      </TaskItemList.EqualDivisionContainer>

      <TaskItemList.EqualDivisionContainer isLast>
        <TaskItemList.EqualDivisionContainer.Title>
          fim
        </TaskItemList.EqualDivisionContainer.Title>
        <TaskItemList.EqualDivisionContainer.Content>
          {dayjs(endTime).format('HH:mm')}
        </TaskItemList.EqualDivisionContainer.Content>
        {!isSystemTask && (
          <div style={{ position: 'absolute', display: 'flex', flexDirection: 'column', right: '5px' }}>
          <IconButton onClick={handleEdit}>
            <RiPencilLine color="white" size={16} />
          </IconButton>
          <IconButton onClick={() => onDelete(name)}>
            <RiDeleteBinLine color="white" size={16} />
          </IconButton>
        </div>
        )}
      </TaskItemList.EqualDivisionContainer>
      {/* </FormControl> */}
    </TaskItemList>
  );
}
