import styled from 'styled-components';

const TaskLiveInfo = styled.div`
  height: 100px;
  width: 1200px;
  padding-top: 100px;
  ${({ isLast }) => isLast && 'margin-bottom: 100px;'}
  border-left: 3px solid #C9AEA7;
  border-bottom: 3px solid ${({ isActive }) => (isActive ? 'white' : '#C9AEA7')};
  color: ${({ isActive }) => (isActive ? 'white' : '#C9AEA7')};
  position: relative;
`;

TaskLiveInfo.TaskPeriod = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  margin: 0 10px 0 10px;
`;

TaskLiveInfo.TaskPeriod.Title = styled.div`
  font-size: 14px;
`;

TaskLiveInfo.TaskPeriod.Content = styled.div`
  font-size: 28px;
  margin-bottom: 7px;
`;

TaskLiveInfo.TaskContainer = styled.div`
  position: absolute;
  right: 0;
  top: 150px;
  background-color: #1F2035;
  width: 500px;
  height: fit-content;
  max-height: 175px;
  border-radius: 20px;
`;

TaskLiveInfo.TaskContainer.Title = styled.div`
  height: 50px;
  width: 100%;
  border-bottom: 3px solid ${({ isActive }) => (isActive ? 'white' : '#C9AEA7')};
  display: flex;
  justify-content: center;
  align-items: flex-end;
  font-size: 24px;
`;

TaskLiveInfo.TaskContainer.Container = styled.div`
  max-height: 110px;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  text-align: justify;
  font-size: 18px;
`;

export default TaskLiveInfo;
