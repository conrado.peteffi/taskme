import React, { useState, useEffect } from 'react';
import dayjs from 'dayjs';
import {
  typeLabelMapper,
} from 'app/utils';
import TaskLiveInfo from './styles';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

const getDuration = (a, b) => Math.abs(Math.floor(a?.diff(b, 'minute', true)));

function EndDialogComponent({
  open,
  handleClose,
  title,
  content,
}) {
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <span>
            {title}
          </span>
        </div>
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
        {content}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Ok</Button>
      </DialogActions>
    </Dialog>
  );
}

function DialogComponent({
  open,
  handleClose,
  title,
  content,
  type,
  breakDuartion,
}) {

  const timeTypeMapper = {
    break: breakDuartion,
    focusTimePlanning: 5,
    pause: 15,
    special: 0,
  }

  const initialTime = dayjs().set('second', 0).set('minute', 0).set('hour', 0);
  const [time, setTime] = useState(initialTime);
  const timeSinceBeginning = getDuration(initialTime, time);

  useEffect(() => {
    setTimeout(() => {
      const newTime = time.add(1, 'second');
      setTime(newTime);
    }, 1000);
  }, [time]);

  const timeToDisplay = time.format('HH:mm:ss');
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <span>
            {title}
          </span>
          <span>
            {timeToDisplay}
          </span>
        </div>
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
        {`${content} Você tem ${timeTypeMapper[type]} minutos!`}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Ok</Button>
      </DialogActions>
    </Dialog>
  );
}

function TaskLikeInfoComponent({
  key,
  isLast,
  liveType,
  isActive,
  type,
  name,
  startTime,
  endTime,
  description,
}) {
  const [open, setOpen] = useState(liveType === 'modal');
  const [endOpen, setEndOpen] = useState(liveType === 'lineEnd' || liveType === 'line');
  const isModalWithCard = type === 'focusTimePlanning' || type === 'pause';

  let period = `${dayjs(startTime).format('HH:mm')} - ${dayjs(endTime).format('HH:mm')}`;
  if (liveType === 'line') {
    period = dayjs(startTime).format('HH:mm')
  } else if (liveType === 'lineEnd') {
    period = dayjs(endTime).format('HH:mm')
  }

  return (
    <>
      <TaskLiveInfo isActive={isActive} isLast={isLast}>
        <TaskLiveInfo.TaskPeriod>
          <TaskLiveInfo.TaskPeriod.Title>
            {liveType === 'line' || liveType === 'lineEnd' ? name : (type === 'break' ? 'Intervalo' : 'Período de Atividade')}
          </TaskLiveInfo.TaskPeriod.Title>
          <TaskLiveInfo.TaskPeriod.Content>
            {period}
          </TaskLiveInfo.TaskPeriod.Content>
        </TaskLiveInfo.TaskPeriod>
        {(liveType === 'card' || isModalWithCard) && (
          <TaskLiveInfo.TaskContainer>
            <TaskLiveInfo.TaskContainer.Title isActive={isActive}>
              <span style={{ padding: '5px' }}>
                {typeLabelMapper[type]}
              </span>
            </TaskLiveInfo.TaskContainer.Title>
            <TaskLiveInfo.TaskContainer.Container>
              <span style={{ padding: '5px 20px 20px 20px' }}>
                {description}
              </span>
            </TaskLiveInfo.TaskContainer.Container>
          </TaskLiveInfo.TaskContainer>
        )}
      </TaskLiveInfo>
      <DialogComponent
        open={open}
        handleClose={() => setOpen(false)}
        title={typeLabelMapper[type]}
        content={description}
        type={type}
        breakDuartion={type === 'break' ? getDuration(startTime, endTime) : undefined}
      />
      <EndDialogComponent
        open={endOpen}
        handleClose={() => setEndOpen(false)}
        title={liveType === 'line' ? 'Início de Rotina' : 'Fim de Rotina'}
        content="Esse é um programa com fins acadêmicos, que visa melhorar a qualidade de vida e aumentar a produtividade dos profissionais que trabalham em Home Office."
        type={type}
      />
    </>
  );
}

export default TaskLikeInfoComponent;
