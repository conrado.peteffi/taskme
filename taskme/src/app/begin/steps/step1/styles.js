import styled from 'styled-components';

const Step1 = styled.div`
  position: relative;
`;

Step1.Container = styled.div`
  width: 970px;
  border-radius: 50px;
  background-color: #1F2035;
  padding: 25px;
  max-height: 75vh;
`;

Step1.Container.Title = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  color: #C9AEA7;
  font-weight: 400;
  font-size: 32px;
  line-height: 41px;
  border-bottom: 5px solid #C9AEA7;
  padding-bottom: 20px;
`;

Step1.Container.Content = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 16px;
`;

Step1.Container.Content.Main = styled.div`
  overflow: auto;
  max-height: 53vh;
`;

Step1.Container.Content.Main.TimeInfo = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

Step1.Container.Content.Main.TimeInfo.Line = styled.div`
  width: 100%;
  height: 1px;
  background-color: #C9AEA7;
`;

Step1.Container.Content.Main.TimeInfo.Content = styled.div`
  padding: 5px;
  min-width: fit-content;
`;

Step1.Container.Content.Buttons = styled.div`
  ${({ isSystemValuesGenerated }) => (!isSystemValuesGenerated && 'min-height: 110px;')}
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  margin-top: 16px;
`;

export default Step1;
