/* eslint-disable max-len */
import React, {
  useState,
} from 'react';
import Step1 from './styles';
import dayjs from 'dayjs';
import {
  Button1,
} from 'app/components';
import {
  TaskItemList,
  DialogComponent,
} from '../components';
import {
  useTaskMe,
} from 'app/hooks';
import {
  RiPencilLine,
  RiDeleteBinLine,
} from 'react-icons/ri';
import IconButton from '@mui/material/IconButton';

function Step1Button({ type, children, onClick }) {
  return (
    <Button1
      type={type}
      height={50}
      fontSize={16}
      style={{ width: '100%' }}
      onClick={onClick}
    >
      {children}
    </Button1>
  );
}

function TimeInfo({ value }) {
  return (
    <Step1.Container.Content.Main.TimeInfo>
      <Step1.Container.Content.Main.TimeInfo.Line />
      <Step1.Container.Content.Main.TimeInfo.Content>
        {value}
      </Step1.Container.Content.Main.TimeInfo.Content>
      <Step1.Container.Content.Main.TimeInfo.Line />
    </Step1.Container.Content.Main.TimeInfo>
  );
}

export default function Step1Component({ goToNextStep, id }) {
  const {
    initialConfig,
    setInitialConfig,
    setValues,
    values,
    currentValues,
    calculateSystemTasks,
    isSystemValuesGenerated,
    applyMock,
  } = useTaskMe();
  const [open, setOpen] = useState(true);
  const [editionObject, setEditionObject] = useState(undefined);
  const handleAddTaskClick = (newValue) => {
    setValues((curValues) => ([...curValues, newValue]));
  };

  const validate = (newValue, editionObject) => {
    let currentCurrentValues = currentValues;
    if (editionObject) {
      currentCurrentValues = currentValues.filter((value) => value.name !== editionObject.editionName);
    }

    const endTimeConflict = currentCurrentValues.find((existingValue) => (newValue?.endTime && ((existingValue.endTime.isSame(newValue?.endTime, 'minute'))
      || (existingValue.startTime.isBefore(newValue?.endTime, 'minute') && existingValue.endTime.isAfter(newValue?.endTime, 'minute')))));
    const startTimeConflict = currentCurrentValues.find((existingValue) => (newValue?.startTime && ((existingValue.startTime.isSame(newValue?.startTime, 'minute'))
      || (existingValue.startTime.isBefore(newValue?.startTime, 'minute') && existingValue.endTime.isAfter(newValue?.startTime, 'minute')))));
    const taskInBetweenConflict = currentCurrentValues.find((existingValue) => (newValue.startTime && existingValue.startTime.isAfter(newValue.startTime, 'minute')) && (newValue?.endTime && existingValue.endTime.isBefore(newValue?.endTime, 'minute')));
    if (currentCurrentValues.some(({ name: existingName }) => existingName.toUpperCase() === newValue?.name?.toUpperCase())) {
      return {
        isValid: false,
        message: `Nome da atividade existente.`,
      };
    }
    if (newValue?.startTime?.isSame(newValue?.endTime, 'minute')) {
      return {
        isValid: false,
        message: `Escolha horários de início e fim diferentes.`,
      };
    } if (startTimeConflict) {
      return {
        isValid: false,
        message: `Existe um conflito no Horário de Início com uma atividade existente. "${startTimeConflict?.name}" começa às ${dayjs(startTimeConflict?.startTime).format('HH:mm:ss')} e termina às ${dayjs(startTimeConflict?.endTime).format('HH:mm:ss')}.`,
      };
    } if (endTimeConflict) {
      return {
        isValid: false,
        message: `Existe um conflito no Horário de Fim com uma atividade existente. "${endTimeConflict?.name}" começa às ${dayjs(endTimeConflict?.startTime).format('HH:mm:ss')} e termina às ${dayjs(endTimeConflict?.endTime).format('HH:mm:ss')}.`,
      };
    } if (taskInBetweenConflict) {
      return {
        isValid: false,
        message: `Existe uma atividade que ocorrerá entre o horário especificado. "${taskInBetweenConflict?.name}" começa às ${dayjs(taskInBetweenConflict?.startTime).format('HH:mm:ss')} e termina às ${dayjs(taskInBetweenConflict?.endTime).format('HH:mm:ss')}.`,
      };
    }
    return { isValid: true };
  };

  return (
    <Step1>
      <Step1.Container>
        <Step1.Container.Title>
          {isSystemValuesGenerated ? 'este é seu planejamento para hoje:' : 'como será sua rotina hoje?'}
        </Step1.Container.Title>
        <Step1.Container.Content>
          <Step1.Container.Content.Main>
            {!!initialConfig && (<TimeInfo value={`início: ${dayjs(initialConfig.startTime).format('HH:mm')}`} />)}
            {currentValues.map(({
              type,
              name,
              startTime,
              endTime,
              isSystemTask,
            }, idx) => (
              <TaskItemList
                key={name}
                isFirst={idx === 0}
                type={type}
                name={name}
                startTime={startTime}
                endTime={endTime}
                onEdit={(editionObject) => {
                  // setEditionObject({
                    // editionTask: type,
                    // editionName: name,
                    // editionStartTime: startTime,
                    // editionEndTime: endTime,
                  // });
                  setEditionObject(editionObject);
                  setOpen(true);
                }}
                onDelete={(name) => setValues((prev) => prev.filter((value) => value.name !== name))}
                isSystemTask={isSystemTask}
              />
            ))}
            {!!initialConfig && (<TimeInfo value={`fim: ${dayjs(initialConfig.endTime).format('HH:mm')}`} />)}
          </Step1.Container.Content.Main>
          <Step1.Container.Content.Buttons isSystemValuesGenerated={isSystemValuesGenerated}>

            {!isSystemValuesGenerated && <Step1Button onClick={() => setOpen(true)}>+ adicionar tarefa</Step1Button>}
            {/* {!isSystemValuesGenerated && (
              <Step1Button onClick={() => {
                applyMock();
              }}>
                MOCK
              </Step1Button>
            )} */}

            <Step1Button onClick={() => {
              if (isSystemValuesGenerated) {
                goToNextStep();
              } else {
                calculateSystemTasks();
              }
            }} type="secondary">{isSystemValuesGenerated ? 'GO!' : 'Calcular Rotina'}</Step1Button>

          </Step1.Container.Content.Buttons>
        </Step1.Container.Content>
      </Step1.Container>
      <DialogComponent
        open={open}
        handleClose={() => setOpen(false)}
        handleChange={handleAddTaskClick}
        initialConfig={initialConfig}
        handleIntialConfig={(value) => setInitialConfig(value)}
        validate={validate}
        editionObject={editionObject}
        setEditionObject={setEditionObject}
        handleEdit={(newValue, oldValue) => {
          const idx = values.map((value) => value?.name).indexOf(oldValue.editionName);
          const newArr = values;
          newArr.splice(idx, 1, newValue);
          setValues(newArr);
        }}
      />
    </Step1>
  );
}
