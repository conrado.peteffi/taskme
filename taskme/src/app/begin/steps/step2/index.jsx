/* eslint-disable max-len */
import React, {
  useEffect,
  useState,
} from 'react';
import dayjs from 'dayjs';
import {
  Button1,
} from 'app/components';
import {
  useTaskMe,
  useLive,
} from 'app/hooks';
import {
  TaskLiveInfo,
} from '../components';
import {
  Step2,
  // VerticalLine,
} from './styles';

// {currentValues.map(({
//   type,
//   name,
//   startTime,
//   endTime,
// }, idx) => (
//   <TaskItemList
//     key={name}
//     isFirst={idx === 0}
//     type={type}
//     name={name}
//     startTime={startTime}
//     endTime={endTime}
//   />
// ))}

export default function Step2Component() {
  const {
    initialConfig,
    // setInitialConfig,
    // setValues,
    currentValues,
    applyMock,
    calculateSystemTasks,
  } = useTaskMe();

  // useEffect(() => {
  //   applyMock();
  // }, []);

  const {
    timeToDisplay,
    liveArr,
    setAdd1Min,
    setAdd15Min,
    setAdd60Min,
  } = useLive();

  const [isAble, setIsAble] = useState(false);

  return (
    <>
      <Step2.Time>
        {timeToDisplay}
      </Step2.Time>
      <Step2>
      <div style={{ display: isAble ? 'initial' : 'none' }}>
          <button onClick={setAdd1Min}>
            add 1 min
          </button>
          <button onClick={setAdd15Min}>
            add 15 min
          </button>
          <button onClick={setAdd60Min}>
            add 60 min
          </button>
        </div>
        <div onClick={() => setIsAble(true)} style={{ fontSize: '12px' }}>allow tests</div>
        {liveArr?.map(({
          type,
          liveType,
          isActive,
          name,
          startTime,
          endTime,
          description,
        }, idx, arr) => (
          <TaskLiveInfo
            key={name}
            liveType={liveType}
            isActive={isActive}
            isLast={idx === arr.length - 1}
            type={type}
            name={name}
            startTime={startTime}
            endTime={endTime}
            description={description}
          />
        ))}
      </Step2>
    </>
  );
}
