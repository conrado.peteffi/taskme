import styled from 'styled-components';

// const VerticalLine = styled.div`
//   position: absolute;
//   height: calc(100vh - 115px);
//   left: 130px;
//   top: -20px;
//   width: 3px;
//   background-color: #C9AEA7;
//   overflow: hidden;
// `;

const Step2 = styled.div`
  position: relative;
  // overflow-y: hidden;
  width: 100%;
  height: 100%;
  margin: 0 0 0 130px;
  
`;

Step2.Time = styled.div`
  position: absolute;
  top: -116px;
  right: 15px;
  font-size: 50px;
`;

export { Step2 };
