import styled from 'styled-components';

const Begin = styled.div`
  ${({ fullHeight }) => fullHeight && 'height: 100%;'}
  width: 100%;
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
`;

export default Begin;
