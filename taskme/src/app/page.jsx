'use client';
import Link from 'next/link';
import React from 'react';
import {
  Button1,
} from 'app/components';
import {
  Planta,
} from 'app/assets';
import { Home } from './styles';

function HomePage() {
  return (
    <Home>
      <Home.Main>
        <Button1 type="tertiary">
          <Link href="/begin">
            Star Work Time
          </Link>
        </Button1>
        <div
          style={{
            margin: '40px 0',
            display: 'flex',
            // alignItems: 'flex-end',
          }}
        >
          <span>todo dia merece ser planejado</span>
        </div>
      </Home.Main>
      <Home.Bottom>
        <Planta />
      </Home.Bottom>
    </Home>
  );
}

export default HomePage;
