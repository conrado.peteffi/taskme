import styled from 'styled-components';

const Home = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
`;

Home.Main = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  font-size: 20px;
  z-index: 1;
`;

Home.Bottom = styled.div`
  z-index: 0;
  position: absolute;
  align-items: center;
  width: 100%;
  left: -20px;
  bottom: -80px;
`;

export default Home;
