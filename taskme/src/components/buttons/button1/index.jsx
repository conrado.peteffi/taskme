import React from 'react';
import Button1 from './styles';

export default function Button1Component({
  children,
  type = 'primary',
  fontSize,
  height,
  style,
  onClick,
}) {
  return (
    <Button1
      type={type}
      fontSize={fontSize}
      height={height}
      style={style}
      onClick={onClick}
    >
      {children}
    </Button1>
  );
}
