import styled from 'styled-components';

const typeProps = {
  backgroundColor: {
    primary: 'rgba(67, 61, 85, 0.2)',
    secondary: '#C9AEA7',
    tertiary: '#1F2035',
  },
  color: {
    primary: 'white',
    secondary: '#1F2035',
    tertiary: 'white',
  },
};

const Button1 = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  outline: none;
  border: none;
  width: ${({ width }) => (width || '500')}px;
  height: ${({ height }) => (height || '125')}px;
  left: 507px;
  background: ${({ type }) => (typeProps.backgroundColor[type])};
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.22);
  border-radius: 50px;
  font-weight: 400;
  font-size: ${({ fontSize }) => (fontSize || '40')}px;
  line-height: 72px;
  display: flex;
  align-items: center;
  text-align: center;
  color: ${({ type }) => (typeProps.color[type])};
  ${({ style }) => (style)}
  &:hover {
    cursor: pointer;
  }
  a {
    color: white;
    text-decoration: none;
    &:hover {
      cursor: pointer;
    }
  }
`;

export default Button1;
